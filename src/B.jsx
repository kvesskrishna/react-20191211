import React, { Component } from "react";
import C from "./C";
class B extends Component {
  state = {};
  render() {
    return (
      <div style={{ background: "green", padding: "20px" }}>
        I am B<button onClick={this.props.increment}>Increment</button>
        <C />
      </div>
    );
  }
}

export default B;
