import React, { Component } from "react";
class LifeCycleB extends Component {
  state = {};
  constructor() {
    super();
    console.log("LifecycleB constructor");
  }
  static getDerivedStateFromProps(props, state) {
    console.log("LifecycleB getDerivedStateFromProps");
    return null;
  }
  componentDidMount() {
    console.log("LifecycleB componentDidMount");
  }
  render() {
    console.log("LifecyleB render");
    return <div>Lifecycle B</div>;
  }
  shouldComponentUpdate(nextprops, nextstate) {
    console.log("LifeCycleB shouldcomponentupdate");
    return true;
  }
  getSnapshotBeforeUpdate(prevprops, prevstate) {
    console.log("LifecycleB getsnapshotbeforeupdate");
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    console.log("LifecycleB componentdidupdate");
  }
}

export default LifeCycleB;
