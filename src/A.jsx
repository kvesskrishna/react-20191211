import React, { Component } from "react";
import B from "./B";
import ValueContext from "./ValueContext";

class A extends Component {
  state = {
    value: 50
  };
  render() {
    return (
      <div style={{ background: "red", padding: "20px" }}>
        <ValueContext.Provider value={this.state.value}>
          I am A, value: {this.state.value}
          <B increment={this.incrementHandler} />
        </ValueContext.Provider>
      </div>
    );
  }
  incrementHandler = () => {
    console.log("clicked");
    this.setState({ value: this.state.value + 1 });
  };
}

export default A;
