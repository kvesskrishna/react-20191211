import { combineReducers, createStore } from "redux";

// actions
export const activateGreet = greeting => ({
  type: "ACTIVATE_GREET",
  greeting
});

export const closeGreet = () => ({
  type: "CLOSE_GREET"
});

// reducer
export const greeting = (state = {}, action) => {
  switch (action.type) {
    case "ACTIVATE_GREET":
      return action.greeting;
      break;
    case "CLOSE_GREET":
      return {};
      break;

    default:
      return state;
      break;
  }
};

export const reducers = combineReducers({
  greeting
});
// store
export function configureStore(initialState = {}) {
  const store = createStore(reducers, initialState);
  return store;
}

export const store = configureStore();
