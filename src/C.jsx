import React, { Component } from "react";
import ValueContext from "./ValueContext";

class C extends Component {
  state = {};
  render() {
    return (
      <div style={{ background: "blue", padding: "20px" }}>
        <ValueContext.Consumer>{context => context}</ValueContext.Consumer>
      </div>
    );
  }
}

export default C;
