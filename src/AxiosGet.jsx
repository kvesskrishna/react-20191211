import React, { Component } from "react";
import axios from "axios";
class AxiosGet extends Component {
  state = {
    users: []
  };
  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then(response => {
        console.log(response.data);
        this.setState({ users: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }
  render() {
    return <div>{this.state.users.map(user => user.name)}</div>;
  }
}

export default AxiosGet;
