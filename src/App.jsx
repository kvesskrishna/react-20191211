// import React from "react";
import React, { Component } from "react";

import { connect } from "react-redux";
import { activateGreet, closeGreet } from "./redux";
class App extends Component {
  // state = {  }
  render() {
    return (
      <div>
        <h3>learn react</h3>
        <h1>{this.props.greeting.title || "Hello World"}</h1>
        {this.props.greeting.title ? (
          <button onClick={this.props.closeGreet}>Exit Greeting</button>
        ) : (
          <button
            onClick={() =>
              this.props.activateGreet({ title: "I am the greeting" })
            }
          >
            Click Me
          </button>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  greeting: state.greeting
});
const mapDispatchToProps = {
  activateGreet,
  closeGreet
};
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
