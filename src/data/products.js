const PRODUCTS = [
  {
    id: 1,
    title: "Shoes",
    price: 299,
    stock: 10,
    image:
      "https://rukminim1.flixcart.com/image/714/857/jpinjbk0/shoe/8/h/4/ar4840-010-9-lotto-black-original-imaewa2sjfnjh6up.jpeg?q=50",
    quantity: 0
  },
  {
    id: 2,
    title: "Shirt",
    price: 99,
    stock: 10,
    image:
      "https://images.sunfrogshirts.com/2017/04/10/m_25575-1491823632821-Gildan-Men-Black-_w91_-front.jpg",
    quantity: 0
  },
  {
    id: 3,
    title: "Bike",
    price: 199999,
    stock: 10,
    image:
      "https://images-na.ssl-images-amazon.com/images/I/51gLWjcR8QL._SX355_.jpg",
    quantity: 0
  }
];

export default PRODUCTS;
