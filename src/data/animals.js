const ANIMALS = [
  {
    name: "Dog",
    image: "https://i.ytimg.com/vi/MPV2METPeJU/maxresdefault.jpg"
  },
  {
    name: "Cat",
    image:
      "https://www.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/02-cat-training-NationalGeographic_1484324.jpg"
  },
  {
    name: "Horse",
    image:
      "https://cdn.britannica.com/96/1296-050-4A65097D/gelding-bay-coat.jpg"
  }
];
export default ANIMALS;
