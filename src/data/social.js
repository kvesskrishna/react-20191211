const SOCIAL = [
  {
    title: "Facebook",
    url: "https://www.facebook.com",
    image: "https://www.facebook.com/images/fb_icon_325x325.png"
  },
  {
    title: "Twitter",
    url: "https://www.twitter.com",
    image:
      "https://cdn2.iconfinder.com/data/icons/social-media-square-set/960/Twitter_Sq-512.png"
  },
  {
    title: "Instagram",
    url: "https://www.instagram.com",
    image:
      "https://workingwithdog.com/wp-content/uploads/2016/05/new_instagram_logo-1024x1024.jpg"
  }
];
export default SOCIAL;
