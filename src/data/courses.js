export const courses = [
  {
    id: 1,
    title: "HTML",
    children: [
      {
        id: 1,
        title: "Head Section"
      },
      {
        id: 2,
        title: "Body Section"
      }
    ]
  },
  {
    id: 2,
    title: "CSS",
    children: [
      {
        id: 1,
        title: "Typography"
      },
      {
        id: 2,
        title: "Box Model"
      }
    ]
  }
];
