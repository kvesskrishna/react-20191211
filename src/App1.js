import React, { Component } from "react";
import Person from "./components/Person";
import Product from "./components/Product";
import "./styles/product.css";
import PRODUCTS from "./data/products";
import ProductTitle from "./components/ProductTitle";
import LifeCycleA from "./LifeCycleA";
import Input from "./Input";
import ControlledComponent from "./controlledComponent";
import SocialItems from "./components/SocialItem";
import Social from "./components/Social";
import Animals from "./components/Animals";
import MyParent from "./components/myParent";
import A from "./A";
import AxiosGet from "./AxiosGet";
import FormValidation from "./components/FormValidation";
import Home from "./components/Home";
import About from "./components/About";
import Users from "./components/Users";
import Nav from "./Nav";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
  Prompt
} from "react-router-dom";
import Courses from "./components/Courses";
import UserDetail from "./components/UserDetail";
import ClassCounter from "./components/ClassCounter";
import HookCounter from "./components/HookCounter";
import HooksWithObject from "./components/HooksWithObject";
import CheckBoxHook from "./components/CheckBoxHook";
import EvenOrOdd from "./components/EvenOrOdd";
import ArrayHook from "./components/ArrayHook";
import Flowers from "./components/Flowers";
import TitleClass from "./components/TitleClass";
import TitleHook from "./components/TitleHook";
import MouseHook from "./components/MouseHook";
import UsersHook from "./components/UsersHook";
class App extends Component {
  state = {
    products: PRODUCTS,
    visitors: 0,
    isLoggedIn: true,
    inputval: ""
  };
  render() {
    return (
      <React.Fragment>
        {/* <UsersHook /> */}
        {/* <MouseHook /> */}
        {/* <TitleClass />
        <TitleHook /> */}
        {/* <Flowers /> */}
        {/* <ArrayHook /> */}
        {/* <EvenOrOdd /> */}
        {/* <CheckBoxHook /> */}
        {/* <HooksWithObject /> */}
        {/* <HookCounter /> */}
        {/* <ClassCounter /> */}
        {/* <Router>
          <Nav />
          <button
            onClick={() => {
              this.setState({ isLoggedIn: !this.state.isLoggedIn });
            }}
          >
            {this.state.isLoggedIn ? "Logout" : "Login"}
          </button>
          <input
            type="text"
            value={this.state.inputval}
            onChange={event => {
              this.setState({ inputval: event.target.value });
            }}
          />
          <Prompt
            when={this.state.inputval.length > 0}
            message="Are you sure to navigate?"
          />

          <Switch>
            <Route path="/" exact component={Home} />
            <Route
              path="/about"
              render={() => {
                return this.state.isLoggedIn ? <About /> : <Redirect to="/" />;
              }}
            />
            <Route path="/users" exact component={Users} />
            <Route path="/users/:id" component={UserDetail} />
            <Route path="/courses" component={Courses} />
          </Switch>
        </Router> */}
        {/* <FormValidation /> */}
        {/* <AxiosGet /> */}
        {/* <A /> */}
        {/* <MyParent /> */}
        {/* <Animals /> */}
        {/* <Social /> */}
        {/* <ControlledComponent />
        <Input />
        <LifeCycleA />
        <h1>App</h1>
        <div className="row">
          {this.state.products.map(product => (
            <Product
              key={product.id}
              product={product}
              clickHandler={() => this.increaseQuantity(product.id)}
            >
              <ProductTitle title={product.title} />
            </Product>
          ))}
        </div>
        visitors: {this.state.visitors}
        <button onClick={this.addVisitors}>Add visitors</button> */}
      </React.Fragment>
    );
  }
  // addVisitors = () => {
  //   console.log("adding visitors..");
  //   this.setState({ visitors: this.state.visitors + 1 });
  // };
  // increaseQuantity = id => {
  //   console.log(id, "value increased");
  //   let products = [...this.state.products];
  //   let filtered = products.filter(product => product.id == id)[0];
  //   console.log(filtered);
  //   filtered.quantity++;
  //   let index = products.indexOf(filtered);
  //   console.log(index);
  //   localStorage.setItem("state", JSON.stringify(products));

  //   this.setState({ products: products });
  // };
}

export default App;
