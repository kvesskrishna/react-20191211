import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { courses } from "./../data/courses";
import Topics from "./Topics";
class Courses extends Component {
  state = {
    courses: courses
  };
  render() {
    return (
      <Router>
        <h3>Courses</h3>
        <ul>
          {this.state.courses.map(course => (
            <li>
              <Link to={`/Courses/${course.id}`}>{course.title}</Link>
            </li>
          ))}
        </ul>
        <Route path="/Courses/:id" component={Topics} />
      </Router>
    );
  }
}

export default Courses;
