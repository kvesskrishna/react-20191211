import React, { useState } from "react";
function Flowers() {
  const [flowers, setFlowers] = useState([
    {
      name: "Rose",
      image:
        "https://images.unsplash.com/photo-1496857239036-1fb137683000?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
      count: 0
    },
    {
      name: "Jasmine",
      image:
        "https://5.imimg.com/data5/FF/EW/MY-38022806/jasmine-flower-500x500.jpg",
      count: 0
    },
    {
      name: "Sunflower",
      image:
        "https://www.johnnyseeds.com/dw/image/v2/BBBW_PRD/on/demandware.static/-/Sites-jss-master/default/dwfd95ba5a/images/products/flowers/01814_01_sunrichorangesum.jpg?sw=387&cx=302&cy=0&cw=1196&ch=1196",
      count: 0
    }
  ]);
  return (
    <div>
      {flowers.map(flower => (
        <div key={flower.name}>
          <img
            onClick={() => {
              //   console.log(flower.name, "was clicked");
              const newflowers = [...flowers]; // a new copy of flowers state 0
              //   console.log("before", newflowers);
              const fobj = newflowers.filter(f => f.name == flower.name)[0]; // get the object of clicked flower
              //   console.log(fobj);
              const findex = newflowers.indexOf(fobj); // find the index of clicked flower object
              //   console.log(findex);
              fobj.count = fobj.count + 1; // increment the count by one
              newflowers[findex] = fobj;
              //   console.log("after", newflowers); // 1
              setFlowers(newflowers); // set state with new flowers array
            }}
            src={flower.image}
            style={{ width: "100px" }}
          />
          <p>
            {flower.name} was clicked {flower.count} times
          </p>
        </div>
      ))}
    </div>
  );
}
export default Flowers;
