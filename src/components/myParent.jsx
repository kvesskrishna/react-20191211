import React, { Component } from "react";
import SiblingA from "./SiblingA";
import SiblingB from "./SiblingB";
class MyParent extends Component {
  state = {
    value: 1
  };
  render() {
    return (
      <div
        style={{
          background: "red",
          padding: "20px"
        }}
      >
        My Parent <br />
        <SiblingA value={this.state.value} clicked={this.clickHandler} />
        <SiblingB value={this.state.value} />
      </div>
    );
  }
  clickHandler = () => {
    this.setState({ value: 2 });
  };
}

export default MyParent;
