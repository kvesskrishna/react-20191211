import React, { useState } from "react";
function HookCounter() {
  const [counter, setCounter] = useState(0);
  const [name, setName] = useState("John");
  return (
    <div>
      Counter value: {counter}
      <br />
      Name value: {name}
      <br />
      <input type="text" value={name} onChange={e => setName(e.target.value)} />
      <button onClick={() => setCounter(counter + 1)}>Increase Counter</button>
    </div>
  );
}
export default HookCounter;
