import React, { Component } from "react";
import Axios from "axios";

class UserDetail extends Component {
  state = {
    user: {}
  };
  componentDidMount() {
    Axios.get(
      `https://jsonplaceholder.typicode.com/users/${this.props.match.params.id}`
    ).then(data => {
      console.log(data);
      this.setState({ user: data.data });
    });
  }
  render() {
    console.log();
    return (
      <div>
        <h2>User Detail</h2>
        <p>{this.state.user.username}</p>
        <p>{this.state.user.email}</p>
        <p>{this.state.user.website}</p>
      </div>
    );
  }
}

export default UserDetail;
