import React, { Component } from "react";
import Axios from "axios";
import { Link } from "react-router-dom";
class Users extends Component {
  state = {
    users: []
  };
  componentDidMount() {
    Axios.get("https://jsonplaceholder.typicode.com/users/").then(data => {
      console.log(data.data);
      this.setState({ users: data.data });
    });
  }
  render() {
    return (
      <div>
        {this.state.users.map(user => (
          <h1 key={user.id}>
            <Link to={`users/${user.id}`}>{user.name}</Link>
          </h1>
        ))}
      </div>
    );
  }
}

export default Users;
