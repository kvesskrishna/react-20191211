import React, { Component } from "react";
import { courses } from "../data/courses";

class Topics extends Component {
  state = {
    courses: courses
  };
  render() {
    const topic = this.state.courses.find(
      ({ id }) => id == this.props.match.params.id
    );
    console.log(topic);
    return (
      <div>
        Topics {this.props.match.params.id}
        <ul>
          {topic.children.map(child => (
            <li>{child.title}</li>
          ))}
        </ul>
      </div>
    );
  }
}

export default Topics;
