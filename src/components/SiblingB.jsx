import React, { Component } from "react";
class SiblingB extends Component {
  state = {};
  render() {
    return (
      <div
        style={{
          background: "blue",
          width: "200px",
          //   display: "inline",
          padding: "20px"
        }}
      >
        Sibling B value: {this.props.value}
      </div>
    );
  }
}

export default SiblingB;
