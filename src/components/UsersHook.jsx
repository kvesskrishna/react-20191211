import React, { useState, useEffect } from "react";
import Axios from "axios";
function UsersHook() {
  const [users, setUsers] = useState([]);
  //   const [clicked, setClicked] = useState("false");
  const [id, setId] = useState("");
  useEffect(() => {
    console.log("use effect called..", id);
    console.log("https://jsonplaceholder.typicode.com/users/" + id);
    setTimeout(() => {
      Axios.get("https://jsonplaceholder.typicode.com/users/" + id).then(
        data => {
          //   console.log(data.data);
          setUsers(data.data);
          console.log(users);
        },
        1000
      );
    });
  }, [id]);
  return (
    <div>
      <div>
        <input type="text" onChange={e => setId(e.target.value)} />
      </div>
      {users.map(user => (
        <h1 key={user.id}>{user.username}</h1>
      ))}
    </div>
  );
}
export default UsersHook;
