import React, { Component } from "react";
import ANIMALS from "../data/animals";

class Animals extends Component {
  state = {
    animals: ANIMALS,
    image: ""
  };
  render() {
    return (
      <div>
        {this.state.animals.map(animal => (
          <button onClick={() => this.clickHandler(animal.image)}>
            {animal.name}
          </button>
        ))}
        <img style={{ width: "100px" }} src={this.state.image} alt="" />
      </div>
    );
  }
  clickHandler = img => {
    this.setState({ image: img });
  };
}

export default Animals;
