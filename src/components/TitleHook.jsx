import React, { useState, useEffect } from "react";
function TitleHook() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("john");

  useEffect(() => {
    console.log("use effect triggered");
    document.title = "Button clicked " + count + " times";
  }, []);

  useEffect(() => {
    console.log(name);
  }, [name]);

  return (
    <div>
      <button onClick={() => setCount(count + 1)}>Click {count}</button>
      <input type="text" value={name} onChange={e => setName(e.target.value)} />
    </div>
  );
}
export default TitleHook;
