import React, { Component } from "react";
class SiblingA extends Component {
  render() {
    return (
      <div
        style={{
          background: "green",
          width: "300px",
          //   display: "inline",
          padding: "20px"
        }}
      >
        Sibling A value: {this.props.value}
        <br />
        <button onClick={this.props.clicked}>Pass</button>
      </div>
    );
  }
}

export default SiblingA;
