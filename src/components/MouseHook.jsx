import React, { useState, useEffect } from "react";
function MouseHook() {
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);
  const getMousePosition = e => {
    console.log("mouse moved");
    setX(e.clientX);
    setY(e.clientY);
  };
  useEffect(() => {
    window.addEventListener("mousemove", getMousePosition);
  }, []);
  return (
    <div>
      Position X: {x} <br />
      Position Y: {y}
    </div>
  );
}

export default MouseHook;
