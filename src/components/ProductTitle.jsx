import React, { Component } from "react";
class ProductTitle extends Component {
  state = {};
  render() {
    return (
      <div>
        <p>{this.props.title}</p>
      </div>
    );
  }
}

export default ProductTitle;
