import React, { Component } from "react";
class Person extends Component {
  state = {};
  render() {
    return (
      <div>
        Name: <span>{this.props.name}</span>
        Age: <span>{this.props.age}</span>
      </div>
    );
  }
}

export default Person;
