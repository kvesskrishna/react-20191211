import React, { Component, Fragment } from "react";
import SOCIAL from "../data/social";
import "./SocialItem.css";
class SocialItems extends Component {
  state = {
    social: this.props.social
  };
  render() {
    // let imgStyle = { width: "100px" };
    // let liStyle = { listStyle: "none" };
    return (
      <Fragment>
        <ul>
          <li className="socialList">
            <a onClick={this.handleClick} href={this.state.social.url}>
              <img className="socialImg" src={this.state.social.image} alt="" />
            </a>
          </li>
        </ul>
      </Fragment>
    );
  }
  handleClick(event) {
    if (window.confirm("Are you sure you want to navigate?")) {
      return true;
    } else {
      event.preventDefault();
    }
  }
}

export default SocialItems;
