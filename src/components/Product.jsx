import React, { Component } from "react";

class Product extends Component {
  state = {
    visitors: 0
  };
  productImg = {
    width: "inherit"
  };
  quantityClass = "";

  render() {
    this.quantityClass =
      this.props.product.quantity >= this.props.product.stock / 2
        ? "text-warning"
        : "text-primary";

    return (
      <div className="col-3 product">
        {this.props.children}
        <img style={this.productImg} src={this.props.product.image} alt="" />
        <p className="title">title: {this.props.product.title}</p>
        <p className="price">$ {this.props.product.price}</p>
        <div>{this.displayQuantity()}</div>
        <p>
          Total Product Price:{" "}
          {this.props.product.quantity * this.props.product.price}{" "}
        </p>
        <button onClick={this.props.clickHandler} className="btn btn-success">
          Add to Cart
        </button>
      </div>
    );
  }
  clickHandler = id => {
    console.log(id, "btn clicked");
  };
  displayQuantity = () => {
    if (this.props.product.quantity === 0) {
      return (
        <p className="badge badge-danger">
          Nothing added! Add this product to cart.
        </p>
      );
    } else {
      return (
        <p className={this.quantityClass}>
          Quantity Ordered: {this.props.product.quantity}
        </p>
      );
    }
  };
  addVisitors = () => {
    console.log("adding visitors..");
    this.setState({ visitors: this.state.visitors + 1 });
  };
}

export default Product;
