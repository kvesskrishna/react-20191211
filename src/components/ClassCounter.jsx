import React, { Component } from "react";
class ClassCounter extends Component {
  state = {
    counter: 0
  };
  render() {
    return (
      <div>
        Counter value : {this.state.counter}
        <button
          onClick={() => this.setState({ counter: this.state.counter + 1 })}
        >
          Increase Count
        </button>
      </div>
    );
  }
}

export default ClassCounter;
