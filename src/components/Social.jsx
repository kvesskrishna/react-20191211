import React, { Component } from "react";
import SOCIAL from "../data/social";
import SocialItems from "./SocialItem";

class Social extends Component {
  state = {
    social: SOCIAL
  };
  render() {
    return (
      <div>
        {this.state.social.map(s => (
          <SocialItems social={s} />
        ))}
        {/* <SocialItems /> */}
      </div>
    );
  }
}

export default Social;
