import React, { useState } from "react";
function CheckBoxHook() {
  const [checked, setChecked] = useState(false);
  console.log(checked);
  const checkBoxStyle = {
    display: checked ? "block" : "none"
  };
  return (
    <div>
      <form style={checkBoxStyle}>
        <input type="text" placeholder="User Name" />
        <br />
        <button>Submit</button>
      </form>
      <input
        type="checkbox"
        checked={checked}
        onChange={e => {
          setChecked(e.target.checked);
        }}
      />
      Toggle Form
    </div>
  );
}
export default CheckBoxHook;
