import React, { Component, Fragment } from "react";

const initialState = {
  fullName: "john",
  userEmail: "john@asdf.com",
  nameError: "",
  emailError: ""
};

class FormValidation extends Component {
  state = initialState;

  handleChange = event => {
    console.log(event.target.name);
    this.setState({ [event.target.name]: event.target.value });
    this.validate();
  };

  validate = () => {
    console.log("in validate");
    if (!this.state.fullName.length > 0) {
      this.setState({ nameError: "Full name is required" });
    } else {
      this.setState({ nameError: "" });
    }
    if (!this.state.userEmail.includes("@")) {
      this.setState({ emailError: "Enter a valid Email" });
    } else {
      this.setState({ emailError: "" });
    }
    return true;
  };
  handleSubmit = event => {
    event.preventDefault();
    this.validate();
    console.log(this.state);
  };
  render() {
    return (
      <Fragment>
        <form onSubmit={this.handleSubmit}>
          User name: {this.state.fullName} <br />
          Email: {this.state.userEmail}
          <div>
            <input
              type="text"
              value={this.state.fullName}
              onChange={this.handleChange}
              onKeyUp={this.validate}
              name="fullName"
            />
            <br />
            <div className="text-danger">{this.state.nameError}</div>
          </div>
          <div>
            <input
              type="text"
              name="userEmail"
              value={this.state.userEmail}
              onChange={this.handleChange}
            />
            <br />
            <div className="text-danger">{this.state.emailError}</div>
          </div>
          <div>
            <button>Submit</button>
          </div>
        </form>
      </Fragment>
    );
  }
}

export default FormValidation;
