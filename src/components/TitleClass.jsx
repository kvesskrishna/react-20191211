import React, { Component } from "react";
class TitleClass extends Component {
  state = { count: 0 };
  componentDidMount() {
    document.title = "Button clicked " + this.state.count + " times";
  }
  componentDidUpdate() {
    document.title = "Button clicked " + this.state.count + " times";
  }
  render() {
    return (
      <div>
        <button onClick={this.clickHandler}>Click {this.state.count}</button>
      </div>
    );
  }
  clickHandler = () => {
    this.setState({ count: this.state.count + 1 });
  };
}

export default TitleClass;
