import React, { useState } from "react";

function EvenOrOdd() {
  const [data, setData] = useState({ number: 0, message: "" });
  return (
    <div>
      Number is: {data.number} <br />
      Message: {data.message} <br />
      <input
        type="text"
        value={data.number}
        onChange={e => {
          setData({
            ...data,
            number: e.target.value,
            message:
              e.target.value % 2 == 0 ? "Number is even" : "Number is odd"
          });
        }}
      />
    </div>
  );
}

export default EvenOrOdd;
