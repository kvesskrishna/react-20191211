import React, { Component } from "react";
import { Link } from "react-router-dom";
class Nav extends Component {
  state = {};
  render() {
    return (
      <div>
        <nav
          style={{
            display: "flex",
            listStyle: "none",
            justifyContent: "space-around"
          }}
        >
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/Users">Users</Link>
          </li>
          <li>
            <Link to="/Courses">Courses</Link>
          </li>
        </nav>
      </div>
    );
  }
}

export default Nav;
