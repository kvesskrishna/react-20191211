import React, { Component } from "react";
import LifeCycleB from "./../src/LifeCycleB";
class LifeCycleA extends Component {
  state = {
    updated: false
  };
  constructor() {
    super();

    console.log("LifecycleA constructor");
  }
  static getDerivedStateFromProps(props, state) {
    console.log("LifecycleA getDerivedStateFromProps");
    return null;
  }
  componentDidMount() {
    console.log("LifecycleA componentDidMount");
  }
  render() {
    console.log("LifecyleA render");
    return (
      <div>
        Lifecycle A
        <LifeCycleB />
        <button onClick={this.updateStatus}>Update State</button>
      </div>
    );
  }
  updateStatus = () => {
    this.setState({ update: !this.state.update });
  };
  shouldComponentUpdate(nextprops, nextstate) {
    console.log("LifeCycleA shouldcomponentupdate");
    return true;
  }
  getSnapshotBeforeUpdate(prevprops, prevstate) {
    console.log("LifecycleA getsnapshotbeforeupdate");
  }
  componentDidUpdate(prevprops, prevstate, snapshot) {
    console.log("LifecycleA componentdidupdate");
  }
}

export default LifeCycleA;
