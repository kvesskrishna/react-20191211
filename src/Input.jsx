import React, { Component } from "react";
class Input extends Component {
  state = {};
  name = "";
  render() {
    return (
      <div>
        <input
          type="text"
          ref={input => {
            return (this.name = input);
          }}
        />
        <br />
        <button onClick={this.uncontrolled}>Uncontrolled comp</button>
      </div>
    );
  }
  uncontrolled = () => {
    console.log(this.name.value);
  };
}

export default Input;
