import React, { Component } from "react";
class ControlledComponent extends Component {
  state = {
    name: ""
  };
  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.name}
          onChange={this.changeNameHandler}
        />
        <button onClick={this.checkstatus}>Check status</button>
      </div>
    );
  }
  changeNameHandler = event => {
    this.setState({ name: event.target.value });
  };
  checkstatus = () => {
    console.log(this.state.name);
  };
}

export default ControlledComponent;
